# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo.meister on 21.01.2016.
#

from openerp import models, api
from openerp.addons.connector_flow.task.abstract_task import AbstractTask

from base64 import b64decode
import simplejson
import logging

_logger = logging.getLogger(__name__)


class TableRowImport(AbstractTask):
    def _row_generator(self, file_data, config=None):
        """Parses a given blob into rows; returns an iterator to rows.
           Has to be implemented in derived classes."""
        raise Exception("Not Implemented")

    def run(self, config=None, file_id=None, async=True, **kwargs):
        if not file_id:
            return
        _logger.info('Starting read of flat file')
        lineno = 0
        chunkno = 0
        file = self.session.env['impexp.file'].browse(file_id)
        rows = self._row_generator(b64decode(file.attachment_id.datas),
                                   config=config)

        chunk_data = []
        for row in rows:
            lineno += 1
            if not row:
                continue
            data = row
            chunk_data.append(self.split_line(data, config))
            if lineno % 50 == 0:
                chunkno += 1
                name = '%s, chunk %d' % (file.attachment_id.datas_fname, chunkno)
                chunk = self.session.env['impexp.chunk'].create({'name': name,
                                                                 'data': simplejson.dumps(chunk_data),
                                                                 'file_id': file.id})
                self.run_successor_tasks(chunk_id=chunk.id, async=async, **kwargs)
                chunk_data = []

            if chunk_data:
                # Process the last entries
                chunkno += 1
                name = '%s, chunk %d' % (file.attachment_id.datas_fname, chunkno)
                chunk = self.session.env['impexp.chunk'].create({'name': name,
                                                                 'data': simplejson.dumps(chunk_data),
                                                                 'file_id': file.id})
                self.run_successor_tasks(chunk_id=chunk.id, async=async, **kwargs)

        file.write({'state': 'done'})

    def split_line(self, data, config=None):
        result = {'raw': data}
        if config and config.get('mapping', False):
            mapping = config['mapping']
            for field in mapping:
                start = mapping[field][0]
                end = start + mapping[field][1]
                if len(data) < start:
                    continue
                if len(data) < end:
                    end = len(data)
                value = data[start:end]
                result[field] = value
        return result

class FlatImport(TableRowImport):
    """Parses a flat file and stores the lines as chunks"""

    def _row_generator(self, file_data, config=None):
        encoding = config.get('encoding', 'utf-8')
        data = file_data.decode(encoding)\
                        .encode('utf-8')\
                        .split("\n")
        return iter(data)


class FlatImportTask(models.Model):
    _inherit = 'impexp.task'

    @api.model
    def _get_available_tasks(self):
        return super(FlatImportTask, self)._get_available_tasks() + [
            ('flat_import', 'Flat File Import')]

    def flat_import_class(self):
        return FlatImport
