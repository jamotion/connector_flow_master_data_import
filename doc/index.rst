Master Data Import based on Connector Flow
==========================================

Task Types
----------

This module adds four new task types:

- `Host File Read Task`_
- `Configurable Csv Import Task`_
- `Flat File import Task`_
- `Master Data Import Task`_

  - `Joining, formatting or evaluating values with tuples`_
  - `Mapping source values to odoo values with lists`_
  - `Using Conditions for value evaluation`_
  - `Adding child entries with dictionaries`_

The following chapters will give you some inputs about configuration and usage of these task types

Host File Read Task
-------------------

Usage
^^^^^

This task type provides features for reading files located in a given folder of the odoo server.
All files be readed and transmitted to other tasks for processing the content.
This task is useful if you provide an ftp server on your odoo server for smarter file processing.
We are using it to import big files containing millions of lines.


Configuration Options
^^^^^^^^^^^^^^^^^^^^^

The following example shows the full configuration options:

.. code-block:: python

  {
      'host': {
          'source_directory': '/<path>/<to>/<files>',
          'move_directory': '/<path/<to>/<folder>/<for>/<processed>/<files>',
          'delete_files': True
      }
  }

The whole configuration for this task type is packed in a **host** Dictionary.

*source_directory*
    Required, The folder where the files to process are located

*move_directory*
    Target folder where files will be move after processing

*delete_files*
    Flag, if processed files will be deleted after processing (True or False)

if both parameters *move_directory* and *delete_files* are given, the files will be deleted, not moved.

Configurable Csv Import Task
----------------------------

Usage
^^^^^

This task type extends the *CSV Import* task type deployed with *control_flow* module.
Because of the *CSV Import* does not allow to configure the csv specific options
like delimiters, text separators and more, this task type will allow it.

Configuration Options
^^^^^^^^^^^^^^^^^^^^^

The following example shows the full configuration options:

.. code-block:: python

    {
        'includes_header': True,
        'encoding':'utf-8',
        'dialect': 'excel',
        'dialect_options': {
            'delimiter': ';',
            'doublequote', True,
            'escapechar', '',
            'quotechar', '"',
            'skipinitialspace', False,
            'strict', False
        },
        'header_definition': (
            '<first col name>',
            '<second col name>',
            '<third col name>',
        )
    }

The configuration parameters are defined as a Dictionary.

*includes_header*
    If True, the first line is used as column headers

*encoding*
    The file encoding, it mostly values to 'cp1252' for european excel export.
    For a full list of values take a look at https://docs.python.org/2/library/codecs.html#standard-encodings.

*dialect*
    Dialects are a predefined set of csv options. The following values are deployed by default:

    - *excel*: comma separated, with " as text quotes
    - *excel_tab*: same as *excel* but with TAB as delimiter

*delimiter*
    A one-character string used to separate fields. It defaults to ','.

*doublequote*
    Controls how instances of quotechar appearing inside a field should themselves be quoted.
    When True, the character is doubled. When False, the escapechar is used as a prefix to the quotechar.
    It defaults to True.

*escapechar*
    A one-character string used by the writer to escape the delimiter if quoting is set to QUOTE_NONE and the quotechar
    if doublequote is False. On reading, the escapechar removes any special meaning from the following character.
    It defaults to None, which disables escaping.

*quotechar*
    A one-character string used to quote fields containing special characters, such as the delimiter or quotechar,
    or which contain new-line characters. It defaults to '"'.

*skipinitialspace*
    When True, whitespace immediately following the delimiter is ignored.
    The default is False.

*strict*
    When True, raise exception Error on bad CSV input. The default is False.

*header_definition*
    Required when *includes_header* is False. A Tuple containing the column names.

Flat File import Task
---------------------

Usage
^^^^^

This task provides possibility to import data from a file with fixed positioning of data. You are able to split the
line by offset and length parameters for every field.

Configuration Options
^^^^^^^^^^^^^^^^^^^^^

The following example shows the full configuration options:

.. code-block:: python

    'encoding':'latin-1',
    'mapping': {
        '<first field>': (0,1),
        '<second field>': (2,3),
        '<third field>': (6,3)
    }

*encoding*
    The file encoding, it mostly values to 'cp1252' for european excel export.
    For a full list of values take a look at https://docs.python.org/2/library/codecs.html#standard-encodings.

*mapping*
    A Dictionary containing field definitions in the following format:
    'field name': (offset, length)

    - *field name*: The name of the field, used by following tasks for accessing the data
    - *offset*: Start position of field in line (0 for the first char, 3 for the fourth char)
    - *length*: Field length

Master Data Import Task
-----------------------

Usage
^^^^^

This task is made for extremely configurable data import with search and update features.

Configuration Options
^^^^^^^^^^^^^^^^^^^^^

The following example shows the full configuration options:

.. code-block:: python

    {
        'model': 'res.partner',
        'options': {
            'date_format': '%Y-%m-%d',
            'time_format': '%H:%M:%S',
            'm2m_mode': 'add',
            'base_url': 'https://jamotion.ch',
            'language': 'en_US',
            'search_operator': '=',
        },
        'extid': 'CustomerNumber',
        'key': {
            'ref':'CustomerNumber'
        },
        'default': {
            'is_company': True,
            'customer': True,
            'lang':'de_DE'
        },
        'mapping': {
            'name': 'CustomerName',
            'ref': 'CustomerNumber',
            'street': 'CustomerStreet',
            'zip': 'CustomerZIP',
            'city': 'CustomerCity',
            'child_ids': {
                'extid': 'ContactNumber',
                'key': {
                    'name': ('{0} {1}', 'Contact First Name', 'Contact Name'),
                    'email': 'Contact eMail',
                },
                'mapping': {
                    'name':('{0} {1}', 'Contact First Name', 'Contact Name'),
                    'phone':'Contact Phone',
                    'email': 'Contact eMail'
                }
            }
        }
    }

*model*
    Object name of target table where data should be imported (e.G. *res.partner*, *product.template*)

*options*
    Options are made for global settings of import. Currently the following options exist:

    - date_format: Format of date values. Default odoo format is *%Y-%m-%d*. See `python-date-format`_ for detailed informations.
    - time_format: Format of time values. Default odoo format is *%H:%M:%S*. See `python-date-format`_ for detailed informations.
    - m2m_mode: *add* (default) will add the given values, *replace* will remove existing values and add the new ones.
    - base_url: A default path for import of binary data (e.g. images) from Website or from file server (e.g. https://jamotion.ch )
    - language: Set the language for create and write commands (used for translated field values)
    - search_operator: Used for record search with *key* parameter (see https://www.odoo.com/documentation/8.0/reference/orm.html#domains )

*extid*
    The extid parameter accepts a field name or a list with conditions or mappings which is used as the id value for an external id
    (e.g. __export__.product_template_123, where 123 is the source field value or result uf mapping or conditional result).

    The extid parameter is allowed on top for the main record and also on child records.
    When the extid parameter is set, the key parameter will be ignored. So please avoid defining an extid and a key.
    Without a defined extid or key all records will be inserted as new records on every run.

*key*
    With the key parameter you could define the search values to determine if a record exists and should be updated
    or if a new record has to be created. The *key* parameter needs a dictionary containing all fields in format
    *'first odoo field name': 'first source field name', 'second odoo field name': 'second source field name'*.

    The key parameter is allowed on top for the main record and also on child records. On child records an automatic
    key value is added for joining the main table with the child table.
    When the extid parameter is set, the key parameter will be ignored. So please avoid defining an extid and a key.
    Without a defined extid or key all records will be inserted as new records on every run.

*default*
    The default parameters needs a dictionary with field definitions like *'odoo field name': <value>*. This
    parameter only allowes fixed values. The values are converted into type of odoo field, if possible.

*mapping*
    This parameter needs a dictionary and will join the source fields with odoo fields. The key (first part
    before *:*) defines the odoo field name, the second part could be one of the following types:

       - String: Containing the source field name
       - Tuple: With tuples it is possible to format or join the source values
         (see `Joining, formatting or evaluating values with tuples`_ for further informations).
       - Dictionary: In case of a one2many or many2many field the dictionary allowes to create child entries (see
         `Adding child entries with dictionaries`_ for further informations).

    Field conversions based on odoo field type:

       - Boolean: The following values will result as True: True, 'True', 'true', 1, -1, 'X', 'x'
       - Date: Date values will be converted from string based on the *date_format* option
       - Time: Date values will be converted from string based on the *time_format* option
       - Datetime: Datetime values will be converted from string based on the *date_format* and *time_format* option combined
       - Many2One: Values will be converted into an integer as ID of dataset
       - One2Many / Many2Many: Values will be converted into a list specially formatted for odoo child entries.
         For Many2Many fields the option *m2m_mode* is used to set if new entries will be added or all existing records will be replaced.

Joining, formatting or evaluating values with tuples
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you need to convert, format or join values of source data you could add a tuple as source field. Every tuple needs
a string containing the layout of result (see string.format_ or formatting.options_ for further informations).

As a special case, you can define another tuple as first value of tuple to evaluate the result of string.format with python code.

Here are some examples:

.. code-block:: python

    {
        'name': ('{0} {1}', 'last name', 'first name')
    }

This example will join both fields *last name* and *first name* separated by space.

.. code-block:: python

    {
        'odoo value': ('{:05.2f}', 'numeric value')
    }

This example will format the numeric value with total length of 5 chars, filled up with leading zero, and 2 digits
after decimal delimiter.

.. code-block:: python

    {
        'name': (('value[:5]', '{0}'), 'Name')
    }

This example is formatting the field value and then uses python code to convert the result:

    1. value = '{0}'.format('The Value of the field')
    2. eval(value[:5])

So the result will be: **'The V'** (the first five chars).

Some good examples of formatting numeric values are described at python-string-format_.

Mapping source values to odoo values with lists
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you need to map a external value into a specific odoo value you could add a list as source field. The first entry
of list has to be the field name as string. All following entries must be of type tuple containing the odoo value
followed by the source value.

Here are some examples:

.. code-block:: python

    [
        'country_id',
        (48, 'CH'),
        (48, 'Switzerland'),
        (89, 'GR'),
        (89, 'Greece'),
    ]

This example will join both fields *last name* and *first name* separated by space.

Using Conditions for value evaluation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have to decide which value is the right for your odoo field, you could also add a list as source field.
The first entry of list has to be the field name as string. The second value must be another list containing the three
values *conditions*, *true expression* and *false expression*.

It is possible to add multiple conditions in odoo domain style (using '|' as OR operator and '&' as AND operator).
The polish notation system is used here. The typical python operators (*==*, *!=*, *>=*, ...) are allowed for comparison.

If you need to compare two field values you have to add the prefix $ on second comparison value so the system will get the field
value instead of the fixed string.

Here are some examples:

.. code-block:: python

    [
        'name': [
            [('firma', '!=', '')],
            ('{0}', 'firma'),
            ('{0} {1}', 'firstname', 'secondname'),
        ],
        'is_company': [
            [('firma', '!=', '')],
            True,
            False,
        ],
        'po_zip': [
            [('plz', '!=', '$plz2')],
            ('{0}', 'plz2'),
            ('{0}', ''),
        ],
    ]

This example will check if the field *firma* is not empty. If it is not empty, the firma name is used, otherwise
 the first name and secondname values are concatenated and used as name value.


Adding child entries with dictionaries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When you need to add some child entries (eg. supplier informations for a product), you could define a dictionary
as source field containing at least a *mapping' key. As on field definition of main model you also could define a
*key* and a *default* key for adding search criteria or default values as described on the introduction above.

.. code-block:: python

    'child_ids': {
            'key': {
                'name': ('{0} {1}', 'Contact First Name', 'Contact Name'),
                'email': 'Contact eMail',
            },
            'mapping': {
                'name':('{0} {1}', 'Contact First Name', 'Contact Name'),
                'phone':'Contact Phone',
                'email': 'Contact eMail'
            }
        }

All the features of first level model are also available on sub models (using tuples or another dictionary).
With this you could add data on multiple child tables in one step.
On every child model a given key is extended (or generated if none is defined) with the connection between both models.

As an example for adding contacts to a company the key ``'parent_id': 123`` is automatically added:

.. code-block:: python

    'key': {
        'name': ('{0} {1}', 'Contact First Name', 'Contact Name'),
        'email': 'Contact eMail',
        'parent_id': 123
    },

.. _string.format: https://docs.python.org/2/library/stdtypes.html#str.format
.. _formatting.options: https://docs.python.org/2/library/string.html#formatstrings
.. _python-string-format:  https://mkaz.github.io/2012/10/10/python-string-format/
.. _python-date-format: http://www.tutorialspoint.com/python/time_strptime.htm