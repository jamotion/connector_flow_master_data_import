Changelog
=========
Version 8.0.1.2.0
-----------------

- Bugfix: Wrong model selection on m2x fields
- Feature: Search operator as option to change the search behavior

Version 8.0.1.1.1
-----------------

- Bugfix 1377: Import Many2Many field as extid not working

Version 8.0.1.1.0
-----------------

- Feature 1007: Error Message if field in mapping not exists
- Bugfix 1007: allow default value False
- Bugfix: Updating record and unit tests